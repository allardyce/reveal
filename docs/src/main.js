import Vue from 'vue'

import App from './App.vue'

import './main.scss'

Vue.config.productionTip = false

import reveal from '../../'
Vue.use(reveal);

new Vue({
  render: h => h(App),
}).$mount('#app')

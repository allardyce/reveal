# @allardyce/reveal

A Vue Directive Reveal Solution

Live Demo: [https://allardyce.gitlab.io/reveal/](https://allardyce.gitlab.io/reveal/)

## Install

```
yarn add @allardyce/reveal
```


## Setup

```
import reveal from '@allardyce/reveal'
Vue.use(reveal)
```

## Usage

#### Default
```
<div v-reveal>Hello World!</div>
```
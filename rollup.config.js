import babel from 'rollup-plugin-babel'
import postcss from 'rollup-plugin-postcss'

export default {

    input: 'src/index.js',

    output: [
        {
            file: 'lib/allardyce-reveal.es.js',
            format: 'es'
        },
        {
            file: 'lib/allardyce-reveal.cjs.js',
            format: 'cjs'
        },
        {
            file: 'lib/allardyce-reveal.js',
            format: 'iife',
            name: 'reveal'
        }
    ],

    external: [ 'intersection-observer' ],

    plugins: [

        postcss(),

        babel(),

    ],

  };
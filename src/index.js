import 'intersection-observer';
import './reveal.scss'

const plugin = {

    install(Vue, os = {}) {     
        
        const observer = new IntersectionObserver((entries, observer) => {
        
            entries.forEach(entry => {
                if (entry.isIntersecting) {
                    const el = entry.target;
                    el.classList.remove(el.scrollTransition);
                    observer.unobserve(el);
                }
            })
        
        }, { threshold: 0.1 });

        Vue.directive('reveal', {
                    
            inserted(el, binding) {

                let transition;
                if (binding.value && binding.value.transition) {
                    transition = `reveal-${binding.value.transition}`;
                } else {
                    transition = 'reveal-fade';
                }
                
                el.classList.add('reveal', transition);

                if (binding.value && binding.value.delay) {
                    el.style.transitionDelay = binding.value.delay;
                }

                el.scrollTransition = transition;

                observer.observe(el);

            }

        })

    }

}

export default plugin;
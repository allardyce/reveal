import 'intersection-observer';

function styleInject(css, ref) {
  if (ref === void 0) ref = {};
  var insertAt = ref.insertAt;

  if (!css || typeof document === 'undefined') {
    return;
  }

  var head = document.head || document.getElementsByTagName('head')[0];
  var style = document.createElement('style');
  style.type = 'text/css';

  if (insertAt === 'top') {
    if (head.firstChild) {
      head.insertBefore(style, head.firstChild);
    } else {
      head.appendChild(style);
    }
  } else {
    head.appendChild(style);
  }

  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    style.appendChild(document.createTextNode(css));
  }
}

var css = ".reveal {\n  transition: all 1s ease;\n}\n.reveal-fade {\n  opacity: 0.01;\n}\n.reveal-fade-slide-left {\n  transform: translateX(-50%);\n}\n.reveal-fade-slide-right {\n  transform: translateX(50%);\n}\n.reveal-fade-slide-up {\n  transform: translateY(50%);\n}\n.reveal-fade-slide-down {\n  transform: translateY(-50%);\n}\n.reveal-fade-grow {\n  transform: scale(0.75);\n}\n.reveal-fade-shrink {\n  transform: scale(1.25);\n}";
styleInject(css);

var plugin = {
  install: function install(Vue) {
    var observer = new IntersectionObserver(function (entries, observer) {
      entries.forEach(function (entry) {
        if (entry.isIntersecting) {
          var el = entry.target;
          el.classList.remove(el.scrollTransition);
          observer.unobserve(el);
        }
      });
    }, {
      threshold: 0.1
    });
    Vue.directive('reveal', {
      inserted: function inserted(el, binding) {
        var transition;

        if (binding.value && binding.value.transition) {
          transition = "reveal-".concat(binding.value.transition);
        } else {
          transition = 'reveal-fade';
        }

        el.classList.add('reveal', transition);

        if (binding.value && binding.value.delay) {
          el.style.transitionDelay = binding.value.delay;
        }

        el.scrollTransition = transition;
        observer.observe(el);
      }
    });
  }
};

export default plugin;
